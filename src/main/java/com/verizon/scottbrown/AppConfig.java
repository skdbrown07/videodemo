package com.verizon.scottbrown;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by scottbrown on 12/24/15.
 */
@Configuration
public class AppConfig extends WebMvcConfigurerAdapter {

    @Value( "${output.path}" )
    private String outputPath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/files/**")
                .addResourceLocations("file:" + this.outputPath);
    }
}
