package com.verizon.scottbrown;

/**
 * Created by scottbrown on 12/24/15.
 */
public class MergeRequest {

    private String name;
    private String input1;
    private String input2;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInput1() {
        return input1;
    }

    public void setInput1(String input1) {
        this.input1 = input1;
    }

    public String getInput2() {
        return input2;
    }

    public void setInput2(String input2) {
        this.input2 = input2;
    }
}
