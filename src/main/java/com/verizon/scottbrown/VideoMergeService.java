package com.verizon.scottbrown;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/**
 * Created by scottbrown on 12/22/15.
 */
@Component
public class VideoMergeService {

    private String inputPath;
    private String outputPath;

    @Autowired
    public VideoMergeService(Environment env) {
        this.inputPath = env.getRequiredProperty("input.path");
        this.outputPath = env.getRequiredProperty("output.path");
    }

    public List<String> getInputFiles() {
        return getFileList(inputPath);
    }

    public List<String> getOutputFiles() {
        return getFileList(outputPath);
    }

    private List<String> getFileList(String dir) {
        File f = new File(dir);
        return Arrays.asList(f.list());
    }

    public String createFile(String name, MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(new File(inputPath + name)));
                stream.write(bytes);
                stream.close();
                return "You successfully uploaded " + name + "!";
            } catch (Exception e) {
                return "You failed to upload " + name + " => " + e.getMessage();
            }
        } else {
            return "You failed to upload " + name + " because the file was empty.";
        }
    }

    public String mergeFiles(String input1, String input2, String name) {
        File dir = new File(outputPath + name);
        if (dir.exists()) {
            throw new IllegalArgumentException("The name already exists, try something else");
        }
        if (!dir.mkdir()) {
            throw new RuntimeException("Unable to create output directory");
        }

        try {
            Process p = Runtime.getRuntime().exec(new String[] {
                    "ffmpeg", "-y", "-i", inputPath + input1, "-i", inputPath + input2, "-filter_complex",
                    "nullsrc=size=640x960 [background]; [0:v] scale=640:480 [resized1]; [1:v] scale=640:480 [resized2]; [background] [resized1] overlay=shortest=1 [black+top]; [black+top] [resized2] overlay=y=480 [top+bottom]",
                    "-map", "[top+bottom]", outputPath+name+ "/out.m3u8"});
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            int exitCode = p.waitFor();
            if (exitCode != 0) {
                // read the output from the command
                System.out.println("Here is the standard output of the command:\n");
                String s = null;
                while ((s = stdInput.readLine()) != null) {
                    System.out.println(s);
                }

                // read any errors from the attempted command
                System.out.println("Here is the standard error of the command (if any):\n");
                while ((s = stdError.readLine()) != null) {
                    System.out.println(s);
                }
                throw new RuntimeException("FFMpeg did not return success.");
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to create file", e);
        } catch (InterruptedException e) {
            throw new RuntimeException("Process did not finish!");
        }

        return name;
    }
}
