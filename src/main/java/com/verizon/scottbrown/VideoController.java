package com.verizon.scottbrown;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by scottbrown on 12/22/15.
 */
@RestController
@RequestMapping(value = "/videos")
public class VideoController {

    @Autowired
    private VideoMergeService mergeService;
    private String homeVideo;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<String> getOutputList() {
        return mergeService.getOutputFiles();
    }

    @RequestMapping(value = "/options", method = RequestMethod.GET)
    public List<String> getOptionsList() {
        return mergeService.getInputFiles();
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String getHomeVideo() {
        if (StringUtils.isEmpty(homeVideo)) {
            List<String> options = mergeService.getOutputFiles();
            if (options.isEmpty()) {
                return "None";
            }
            return options.get(0);
        } else {
            return homeVideo;
        }
    }

    @RequestMapping(value = "/home", method = RequestMethod.PUT)
    public void setHomeVideo(@RequestBody String name) {
        // TODO: validation
        this.homeVideo = name;
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String handleFileUpload(@RequestParam("name") String name,
                                   @RequestParam("file") MultipartFile file) {
        return mergeService.createFile(name, file);
    }

    @RequestMapping(value = "/merge", method = RequestMethod.POST)
    public String mergeFiles(@RequestBody MergeRequest request) {
        return mergeService.mergeFiles(request.getInput1(), request.getInput2(), request.getName());
    }
}